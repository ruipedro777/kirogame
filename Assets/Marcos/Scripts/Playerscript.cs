﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Playerscript : MonoBehaviour
{
    public bool healthtaken;

    [SerializeField] Transform spawnpoint;

    [HideInInspector] public static int health=3;
    //public int numofhearths;
    public float jumphigh;
    public float runspeed;

    public Image[] hearts;

    private Rigidbody2D rg2d;
    private BoxCollider2D bc2d;
    private bool _canjump = false;

    private Animator anim;
    
    // Start is called before the first frame update
    void Start()
    {
        health = FindObjectOfType<GameManager>().GetHealth();
        rg2d = transform.GetComponent<Rigidbody2D>();
        bc2d = transform.GetComponent<BoxCollider2D>();

        anim = GetComponent<Animator>();
    }
   

    // Update is called once per frame
    void Update()
    {
        ff
        healthtaken = false;
        
        
        Vector3 movement = new Vector3(Input.GetAxis("Horizontal"), 0f, 0f);
        transform.position += movement * Time.fixedDeltaTime * runspeed;
        MoveCharacter(movement);


     

        if(_canjump && Input.GetKeyDown(KeyCode.Space)  )

        {
            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, jumphigh), ForceMode2D.Impulse);

             anim.set("idle", true);

            
        }
        Debug.Log(_canjump);
        
        
            for (int i = 0; i < hearts.Length; i++)
            {
                if (i < health)
                {
                    hearts[i].gameObject.SetActive(true);
                }
                else
                {
                    hearts[i].gameObject.SetActive(false);
                }
            }
        if (health <= 0)
        {
            FindObjectOfType<GameManager>().EndGame();
            

        }


        // se passar fora do cenário, morre
        /*if (rg2d.position.y < -8f)
        {
            transform.position = spawnpoint.position;
            if(healthtaken == false)
            {
                healthtaken = true;
                FindObjectOfType<Activatewhenrestart>().Reset();
                health -= 1;
                
            }
            
            
        }*/
    }
    /*public bool Isfalling
    {
        get{
            Debug.Log(rg2d.velocity.y < 0);
            return rg2d.velocity.y < 0;
           
        }
        
    }*/
    public void MoveCharacter(Vector3 movement)
    {
        
        
        if(movement.x > 0)
        {
            
            transform.rotation = Quaternion.Euler(transform.localRotation.x, 0, transform.localRotation.z);
            //transform.Rotate(0f, 180f, 0f);
        }
        else if(movement.x < 0)
        {
            transform.rotation = Quaternion.Euler(transform.localRotation.x, 180, transform.localRotation.z);
            //transform.Rotate(0f, 180f, 0f);
        }

    }

    
    void OnCollisionEnter2D(Collision2D col)
    {
        /*if (col.gameObject.CompareTag("Hazard")) // Colidimos com algo que nos deve matar instantaneamente?
        {
            Die();
        }*/
        if (col.gameObject.CompareTag("Ground"))
        {
            _canjump = false;
            foreach (ContactPoint2D point in col.contacts) // Se � algo que n�o nos mata...
            {
                if (point.normal.normalized.y > 0.5)// � um plano horizontal? Isto �, a normal dos pontos de colis�o aponta para cima?
                {
                    _canjump = true; // Se sim, podemos saltar!
                }
            }
        }
        if (col.gameObject.CompareTag("Platforms"))
        {
            _canjump = false;
            foreach (ContactPoint2D point in col.contacts) // Se � algo que n�o nos mata...
            {
                if (point.normal.normalized.y > 0.5)// � um plano horizontal? Isto �, a normal dos pontos de colis�o aponta para cima?
                {
                    _canjump = true; // Se sim, podemos saltar!
                    transform.parent = col.gameObject.transform;
                }
            }
        }
        if (col.gameObject.CompareTag("Enemy"))
        {
            if (healthtaken == false)
            {
                //Invoke("FindObjectOfType<GameManager>().Restart()", 4);
                FindObjectOfType<GameManager>().Restart();
                health -= 1;
                healthtaken = true;
            }
            
            transform.position = spawnpoint.position;
        }



    }
    void OnCollisionStay2D(Collision2D col)
    {
        if (col.gameObject.CompareTag("Ground") ||col.gameObject.CompareTag("Platforms"))
        {
            _canjump = false;
            foreach (ContactPoint2D point in col.contacts) //Mesma coisa que o OnCollisionEnter2D :)
            {
                if (point.normal.normalized.y > 0.5)
                {
                    _canjump = true;
                }
            }
        }
        
    }
    void OnCollisionExit2D(Collision2D col) // Se estamos a cair ou saltar, ou seja, n�o estamos a colidir com nada
    {
        if (col.gameObject.CompareTag("Ground"))
        {
            _canjump = false;
            foreach (ContactPoint2D point in col.contacts)
            {
                if (point.normal.normalized.y > 0.01)
                {
                    _canjump = true;

                }
            }
        }
        if (col.gameObject.CompareTag("Platforms"))
        {
            _canjump = false;
            transform.parent = null;
            foreach (ContactPoint2D point in col.contacts)
            {
                if (point.normal.normalized.y > 0.5)
                {
                    _canjump = true;

                }
            }
        }
        if (col.gameObject.CompareTag("Enemy"))
        {
            
                healthtaken = false;
            

            
        }

    }
}
