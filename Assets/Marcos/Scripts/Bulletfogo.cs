﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bulletfogo : MonoBehaviour
{
    public float speed ;
    public float bulletforce;
    private Rigidbody2D rb;

    // Start is called before the first frame update
    void Start( )
    {
        
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = transform.right * speed;
        
    }

    // Update is called once per frame
    void Update()
    {
       
        
        
    }
    
    private void OnCollisionEnter2D(Collision2D collision)
    {


        if (collision.gameObject.CompareTag(MyTags.Box_tag)) //se colidir com a caixa
        {
            collision.gameObject.SetActive(false);
            //chamar animacao do fogo da caixa
        }
        if (collision.gameObject.CompareTag("Box"))
        {
            
            
            collision.gameObject.SetActive(false);

            Destroy(gameObject);
        }
        Destroy(gameObject);



    }
    
}

