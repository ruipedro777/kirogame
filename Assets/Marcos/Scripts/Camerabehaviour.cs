﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camerabehaviour : MonoBehaviour
{
    public Transform  target;
    public float smoothing;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(transform.position != target.position)
        {

            Vector3 targetposition = new Vector3(target.position.x+10, target.position.y+7, transform.position.z);
            transform.position = Vector3.Lerp(transform.position, targetposition, smoothing);
        }
    }
}
