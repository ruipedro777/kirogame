﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public static int health=3; // vidas iniciais

   

    public bool gameover;
    public float intdelay;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void EndGame() //chama o menu de fim do jogo
    {
        if (health <= 0) //se o numero de vidas chega a zero
        {
            gameover = true;
            Debug.Log("Gameover");
            Invoke("Restart", intdelay); // chama a a funcao restart e com um tempo de delay pre estabelecido
        }
    }

    public void Restart()
    {
        if (gameover)
        {

            SceneManager.LoadScene(SceneManager.GetActiveScene().name); // chama a funcao inicial
            health = 3; // as vida vidas a 3
        }
        else
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name); //quanto nao chamar o endGame 
            health--; //reduz o numero de vidas
        }

    }


        public int GetHealth()
        {
            return health;
        }
    
}
