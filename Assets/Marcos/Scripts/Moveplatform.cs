﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moveplatform : MonoBehaviour
{

    private Vector3 PosA; // vectores de onde e para onde a plataforma vai mover
    private Vector3 PosB;

    private Vector3 Nextposition; 


    [SerializeField]
    private float speed;

    public Transform childTransform;
    public Transform transformB;

    


    // Start is called before the first frame update

    void Start()
    {
        PosA = childTransform.localPosition;
        PosB = transformB.localPosition;
        Nextposition = PosB;
    }


    

    // Update is called once per frame

    void Update()
    {
        Move();
    }



    private void Move()
    {
        childTransform.localPosition = Vector3.MoveTowards(childTransform.localPosition, Nextposition, speed * Time.deltaTime);
        if(Vector3.Distance(childTransform.localPosition,Nextposition) <= 0.1f)
        {
            Changedestination();
        }
    }


    

    private void Changedestination()//mudar de direccao 

    {
        if( Nextposition != PosA)
        {
            Nextposition = PosA;
        }
        else
        {
            Nextposition = PosB;
        }
    }
    /*private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.gameObject.layer = 8;
            collision.transform.SetParent(childTransform);
        }
    }
    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.gameObject.layer = 8;
            collision.transform.SetParent(childTransform);
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        collision.transform.SetParent(null);
    }*/
}
