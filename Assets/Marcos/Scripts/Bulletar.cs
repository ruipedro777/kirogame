﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bulletar : MonoBehaviour
{
    public float speed;
    public float bulletforce;
    public Rigidbody2D rb;


    void Start()  
    {
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = transform.forward * speed;
        /*
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = transform.right * speed;
        */
    }


    void Update()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = transform.right*speed;

    }
    
    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.CompareTag(MyTags.Box_tag)) // se colidir com a caixa
        {
            
            
            collision.rigidbody.AddForce(new Vector2(bulletforce, 0)); //aplica uma forca a caixa no eixo dos x
            Destroy(gameObject);
        }
        Destroy(gameObject);

        if (collision.gameObject.CompareTag(MyTags.Enemy_tag)) //se for atirado ar para o inimigo
        {
            //spawn more enemies
          
        }

        if (collision.gameObject.CompareTag("Box"))
        {
           
            
            collision.rigidbody.AddForce(new Vector2(bulletforce, 0));
            Destroy(gameObject);
        }
        Destroy(gameObject);

        
    }
    
}
