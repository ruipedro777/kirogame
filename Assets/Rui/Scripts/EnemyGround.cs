﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGround : MonoBehaviour
{
    public float moveSpeed = 1f;
    private Rigidbody2D myBody;

    private Animator anim;  

    private bool moveLeft; //movimento Inicial

    public Transform down_collision, left_collision, right_collision, top_collision; // para podermos criar um ponto de colisao
    private Vector3 left_Collision_Pos, right_Collision_Pos;


    private bool canMove;
    private bool Stunned; //o jogador deixou de jogador



    public LayerMask playerLayer;

    private void Awake()
    {
        myBody = GetComponent<Rigidbody2D>();
        anim = getComponent<Animator>();

        left_collision.position = left_Collision_Pos; // no momento em que a direcao posicao do eixo dos x troca a posicao de colisao tambem deve trocar
        right_collision.position = right_Collision_Pos; 



    }
    void Start()
    {
        moveLeft = true; // inicalmente o inimigo comeca a desclocacao no eixo dos x para a esquerda
        canMove = true; //inicalmente pode mover
        
    } 

    void Update()
    {
        if (canMove) // se nao esta Stunned
        {
            if (moveLeft)
            {
                myBody.velocity = new Vector2(-moveSpeed, myBody.velocity.y);

                
            }
            else
            {
                myBody.velocity = new Vector2(moveSpeed, myBody.velocity.y);
            }
        }
        

        CheckCollision();
    }

    void CheckCollision()
    {
        



        if ( !Physics2D.Raycast(down_collision.position, Vector2.down, 0.1f)) //cria um vector vertical na direcao inferior e com o tamanho 0.1f e deteta se existe chao
        {
            ChangeDirection();       //se nao detetar chao o movimento vai ser igual ao oposto movimento
        }
    }

    void ChangeDirection()  //muda a direcao do olhar 
    {
        moveLeft = !moveLeft; //muda o movimento do eixo dos x

        Vector3 tempScale = transform.localScale;

        if (moveLeft)
        {
            tempScale.x = Mathf.Abs(tempScale.x); //mathf.abs == vai fazer com que o valor de tempScale.x seja sempre positivo

            left_collision.position = left_Collision_Pos;
            right_collision.position = right_Collision_Pos;
        }
        else
        {
            tempScale.x = -Mathf.Abs(tempScale.x);

            left_collision.position = right_Collision_Pos;  // se a direcao mudar a posicao de colisao tambem tem que mudar
            right_collision.position = left_Collision_Pos;
        }

        transform.localScale = tempScale; //reasigna a nova variavel com um novo valor de localScale


    }

    //IEnumerator Dead(float timer)
    //{
    //    yield return new WaitForSeconds(timer);
    //    gameObject.SetActive(false);
    //}




    //private void OnTriggerEnter2D(Collider2D collision)
    //{
    //    if (collision.tag == MyTags.Bullet_fogo)
    //    {
    //        if(tag == MyTags.Enemy_tag)
    //        {
    //            //anim.Play("");  rola a animacao

    //            canMove = false;
    //            myBody.velocity = new Vector2(0, 0);

    //            StartCoroutine(Dead(0.4f));
    //        }
    //    }
    //}




}//class
